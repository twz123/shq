package shq_test

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"slices"
	"strings"
	"testing"

	"codeberg.org/twz123/shq"
	"codeberg.org/twz123/shq/internal/testutil"
)

var tests = []struct{ name, input, wanted string }{
	{"empty_string", "", "''"},
	{"single_space", " ", "' '"},
	{"simple", "simple", "simple"},
	{"with_space", "with space", "'with space'"},
	{"with_tab", "with\ttab", "'with\ttab'"},
	{"with_newline", "newline\nhere", "'newline\nhere'"},
	{"with_ampersand", "special&char", "'special&char'"},
	{"with_pipe", "pipe|command", "'pipe|command'"},
	{"with_redirect_stdout", "redirect>file", "'redirect>file'"},
	{"with_command", "$(command)", "'$(command)'"},
	{"with_backticks", "`backticks`", "'`backticks`'"},
	{"with_double_quotes", `"double quotes"`, `'"double quotes"'`},
	{"with_single_quotes", "'single quotes'", `"'single quotes'"`},
	{"mixed_special_chars", "mixed &>|`\"'", "'mixed &>|`\"'\"'\""},
	{"job_control", "start&", "'start&'"},
	{"history", "!history", "'!history'"},
	{"braces", "{brace}", "'{brace}'"},
	{"brace_expansion", "{brace,expansion}", "'{brace,expansion}'"},
	{"var_subst", `"${youQuoteMe}"`, `'"${youQuoteMe}"'`},
	{"quoted_var_subst", `'"${youQuoteMe}"'`, `"'\"\${youQuoteMe}\"'"`},
	{"brackets", "[bracket]", "'[bracket]'"},
	{"star", "*star?", "'*star?'"},
	{"tilde", "~tilde", "'~tilde'"},
	{"backslash", `\backslash`, `'\backslash'`},
	{"newline", "\n", "'\n'"},
	{"newline_prefixed", "\nline", "'\nline'"},
	{"newline_space_prefixed", "\n line", "'\n line'"},
	{"extended_ascii", "\x87", "'\x87'"},
	{"single_and_double_quote", `'"`, `"'\""`},
	{"mixed_line_endings", "mixed\nline\r\nendings\r", "'mixed\nline\r\nendings\r'"},
	{"double_single_double", "'`'", "\"'\"'`'\"'\""},
	{"single_double_single", "`'`", "'`'\"'\"'`'"},
}

func TestXoxo(t *testing.T) {
	yolo := "'mixed &>|`\"'\"'\""

	var out bytes.Buffer
	cmd := exec.Command("/nix/store/j2k2rk5d6vwqkiqs3k929f792zk0pbny-fish-3.7.0/bin/fish", "-c", "printf %s "+yolo)
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		t.Fatal(err)
	}

	s := out.String()
	if wanted := "mixed &>|`\"'"; s != wanted {
		t.Errorf("wanted: %s   -> %v", wanted, []byte(wanted))
		t.Errorf("got:    %s -> %v", s, out.Bytes())
	}
}

func TestQuote(t *testing.T) {
	t.Parallel()

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			if strings.HasPrefix(test.wanted, "-") {
				t.Errorf("Expected strings may not start with a hyphen for compatibility reasons with the roundtrip test, which uses `echo`: %q", test.wanted)
			}

			result, err := shq.Quote(test.input)
			if err != nil {
				t.Fatalf("Unexpected error for %q: %v", test.input, err)
			}

			if result != test.wanted {
				t.Errorf("Wanted %q, got %q", test.wanted, result)
			}
		})
	}

	for _, test := range []struct {
		name, input, wanted string
		at                  int
	}{
		{"rejects_nul_byte", "the \x00 byte", "NUL bytes", 4},
		{"rejects_quoted_nul_byte", "the '\x00' byte", "NUL bytes", 5},
		{"rejects_quoted_control_byte", "the '\x7f' byte", "control byte 0x7f", 5},
	} {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			quoted, err := shq.Quote(test.input)
			if quoted != "" {
				t.Errorf("Wanted the empty string, got %q", quoted)
			}
			if err == nil {
				t.Error("Expected an error, got nil")
			} else {
				if prefix, got := fmt.Sprintf("byte %d: %s", test.at, test.wanted), err.Error(); !strings.HasPrefix(got, prefix) {
					t.Errorf("Wanted prefix %q, got %q", prefix, got)
				}
				if err = errors.Unwrap(err); err == nil {
					t.Error("Expected to unwrap an error, got nil")
				} else if substring, got := test.wanted, err.Error(); !strings.Contains(got, substring) {
					t.Errorf("Wanted substring %q, got %q", substring, got)
				}
			}
		})
	}
}

func TestQuoteAll_CatenatesEverything(t *testing.T) {
	t.Parallel()

	var input, wanted []string
	for _, test := range tests {
		input = append(input, test.input)
		wanted = append(wanted, test.wanted)
	}

	quoted, err := shq.QuoteAll(input...)
	if err != nil {
		t.Error("Unexpected error", err)
	} else if wanted, got := strings.Join(wanted, " "), quoted; wanted != got {
		t.Errorf("Wanted %q, got %q", wanted, got)
	}
}

func TestQuoteAll_PropagatesErrors(t *testing.T) {
	t.Parallel()

	quoted, err := shq.QuoteAll("\x00")
	if quoted != "" {
		t.Errorf("Wanted the empty string, got %q", quoted)
	}
	if err == nil {
		t.Error("Expected an error, got nil")
	} else if prefix, got := "word 0: byte 0: NUL bytes", err.Error(); !strings.HasPrefix(got, prefix) {
		t.Errorf("Wanted prefix %q, got %q", prefix, got)
	}
}

func TestQuoteTo_IOErrors(t *testing.T) {
	t.Parallel()

	t.Run("when_inserting_whitespace", func(t *testing.T) {
		t.Parallel()

		triggered, theErr := false, errors.New(t.Name())
		var writer stringWriterFn = func(s string) error {
			if triggered {
				t.Fatalf("Unexpected write: %q", s)
			}
			if s == " " {
				triggered = true
				return theErr
			}
			return nil
		}

		err := shq.QuoteTo(writer, "", "")
		if !triggered {
			t.Errorf("Write error wasn't triggered")
		}
		if got := err; theErr != got {
			t.Fatal("Wanted the test error, got", got)
		}
	})

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			theErr := errors.New(t.Name())
			for allowedWrites := uint(0); ; allowedWrites++ {
				var numWrites uint
				var writer stringWriterFn = func(s string) error {
					if allowedWrites < numWrites {
						t.Fatalf("Unexpected write #%d: %q", numWrites, s)
					}

					numWrites++
					if allowedWrites < numWrites {
						return theErr
					}

					return nil
				}

				err := shq.QuoteTo(writer, test.input)
				if numWrites <= allowedWrites {
					if err != nil {
						t.Fatalf("Unexpected error for %q: %v", test.input, err)
					}
					t.Logf("Passed after %d writes", numWrites)
					return
				}

				if got := err; theErr != got {
					t.Fatal("Wanted the test error, got", got)
				}
			}
		})
	}
}

func BenchmarkQuoteTo(b *testing.B) {
	var buf bytes.Buffer
	for _, test := range tests {
		buf.WriteString(test.input)
	}

	input := buf.String()
	// buf.Grow(len(input))

	var sum uint64
	for i := 0; i < b.N; i++ {
		buf.Reset()
		if err := shq.QuoteTo(&buf, input); err != nil {
			b.Fatalf("Unexpected error: %v", err)
		}
		sum += uint64(len(input))
	}

	b.Log(sum)
}

func FuzzQuote_ShellExec(f *testing.F) {
	shellEcho := shellEchoerFromArgs(os.Args)
	if shellEcho == nil {
		f.Fatalf("No shell given on command line: %v", os.Args)
	}

	for _, test := range tests {
		f.Add(test.input)
	}

	fuzzQuote_Roundtrip(f, func(quoted string) (string, error) {
		echoed, err := shellEcho(quoted)
		if err != nil {
			return "", fmt.Errorf("shell echo failed: %w", err)
		}
		return echoed, nil
	})
}

func FuzzQuote_Wordexp(f *testing.F) {
	if _, err := testutil.Wordexp(""); err != nil {
		f.Fatalf("wordexp(3) not functional: %v", err)
	}

	fuzzQuote_Roundtrip(f, func(quoted string) (string, error) {
		words, err := testutil.Wordexp(quoted)
		if err != nil {
			return "", fmt.Errorf("wordexp(3) failed: %w", err)
		}
		if numWords := len(words); numWords != 1 {
			return "", fmt.Errorf("wordexp(3) didn't expand the quoted word into a single word, but %d", numWords)
		}
		return words[0], nil
	})

}

func fuzzQuote_Roundtrip(f *testing.F, eval func(string) (string, error)) {
	for _, test := range tests {
		f.Add(test.input)
	}

	f.Fuzz(func(t *testing.T, input string) {
		t.Parallel()

		quoted, err := shq.Quote(input)
		if err != nil {
			t.Skipf("Skipping invalid fuzzed input: input=%q (%v), err=%v", input, []byte(input), err)
		}

		if word, err := eval(quoted); err != nil {
			t.Errorf("Evaluation of quoted word failed: input=%q (%v), quoted=%q, err=%v", input, []byte(input), quoted, err)
		} else if word != input {
			t.Errorf("Roundtrip didn't preserve input: input=%q (%v), quoted=%q, word=%q (%v)", input, []byte(input), quoted, word, []byte(word))
		}
	})
}

func shellEchoerFromArgs(args []string) func(value string) (string, error) {
	var shellCmd []string
	for i, arg := range os.Args {
		if arg == "--" {
			shellCmd = os.Args[i+1:]
		}
	}

	if len(shellCmd) < 2 {
		return nil
	}

	if lookedUp, err := exec.LookPath(shellCmd[0]); err != nil {
		panic(err)
	} else {
		shellCmd[0] = lookedUp
	}

	return func(quoted string) (string, error) {
		shellCmd := slices.Clone(shellCmd)
		shellCmd[len(shellCmd)-1] = shellCmd[len(shellCmd)-1] + " " + quoted

		var out strings.Builder
		cmd := &exec.Cmd{
			Path:   shellCmd[0],
			Args:   shellCmd,
			Stdout: &out,
			Env:    []string{},
		}

		if err := cmd.Run(); err != nil {
			return "", fmt.Errorf("failed to execute shell %#v: %w", shellCmd, err)
		}

		s := out.String()
		if len(s) < 1 || s[len(s)-1] != '\n' {
			return "", fmt.Errorf("no trailing newline: %q (%v)", s, []byte(s))
		}

		return s[:len(s)-1], nil
	}
}

type stringWriterFn func(string) error

func (f stringWriterFn) WriteString(s string) (n int, err error) {
	if err := f(s); err != nil {
		return 0, err
	}
	return len(s), nil
}
