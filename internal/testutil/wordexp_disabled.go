//go:build !wordexp

package testutil

import "errors"

func Wordexp(pattern string) ([]string, error) {
	return nil, errors.New("built without wordexp")
}
