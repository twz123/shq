//go:build wordexp

package testutil

/*
#include <wordexp.h>
#include <stdlib.h>

static char *wordexp_getword(const wordexp_t *w, size_t n) { return w->we_wordv[n]; }
*/
import "C"

import (
	"fmt"
	"unsafe"
)

// The wordexp(3) C library function. Performs a shell-like expansion of line
// and returns the expanded words. Command substitution is disabled and will
// result in an error. Attempts to substitute undefined shell variables will
// result in an error.
// https://www.man7.org/linux/man-pages/man3/wordexp.3.html
func Wordexp(line string) ([]string, error) {
	cLine := C.CString(line)
	defer C.free(unsafe.Pointer(cLine))

	var we C.wordexp_t

	ret := C.wordexp(cLine, &we, C.WRDE_NOCMD|C.WRDE_UNDEF)
	defer C.wordfree(&we) // should be called for (some) errors as well
	if ret != 0 {
		return nil, wordexpError(ret)
	}

	words := make([]string, we.we_wordc)
	for i := range words {
		words[i] = C.GoString(C.wordexp_getword(&we, C.size_t(i)))
	}

	return words, nil
}

type wordexpError int

func (e wordexpError) Error() string {
	switch e {
	case C.WRDE_BADCHAR:
		return "illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }"
	case C.WRDE_BADVAL:
		return "an undefined shell variable was referenced"
	case C.WRDE_CMDSUB:
		return "command substitution requested"
	case C.WRDE_NOSPACE:
		return "out of memory"
	case C.WRDE_SYNTAX:
		return "shell syntax error, such as unbalanced parentheses or unmatched quotes"
	default:
		return fmt.Sprintf("wordexpError(%d)", int(e))
	}
}
