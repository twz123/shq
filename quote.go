package shq

import (
	"fmt"
	"io"
	"strings"
)

// Returns a representation of word that will be interpreted as word when parsed
// by a UNIX-like shell. Returns an error if word contains bytes that might be
// illegal in the context of a shell.
func Quote(word string) (string, error) {
	if word == "" {
		return "''", nil
	}

	var quoted strings.Builder
	if neededToQuote, err := quoteTo(&quoted, word); err != nil {
		return "", err
	} else if !neededToQuote {
		return word, nil
	}

	return quoted.String(), nil
}

// Returns a string that, when parsed by UNIX-like shells, will reproduce as the
// list of given words. Returns an error if any of the words contain bytes that
// may be illegal in the context of a shell.
func QuoteAll(words ...string) (string, error) {
	var quoted strings.Builder
	if err := QuoteTo(&quoted, words...); err != nil {
		return "", err
	}
	return quoted.String(), nil
}

// Writes a string to quoted that, when parsed by UNIX-like shells, will
// reproduce as the list of given words. Returns an error if any of the words
// contain bytes that may be illegal in the context of a shell, or if an I/O
// error occurs. In these cases, the quoted words may have been partially
// written to quoted.
func QuoteTo(quoted io.StringWriter, words ...string) error {
	for pos, word := range words {
		if pos > 0 {
			if _, err := quoted.WriteString(" "); err != nil {
				return err
			}
		}
		if word == "" {
			if _, err := quoted.WriteString("''"); err != nil {
				return err
			}
			continue
		}

		if neededToQuote, err := quoteTo(quoted, word); err != nil {
			if _, ok := err.(*wordErr); ok {
				return fmt.Errorf("word %d: %w", pos, err)
			}
			return err
		} else if !neededToQuote {
			if _, err := quoted.WriteString(word); err != nil {
				return err
			}
		}
	}

	return nil
}

// Writes the quoted word to quoted. Returns false if if word contains only
// regular bytes that don't need to be quoted. In that case, nothing is written
// to quoted at all.
func quoteTo(quoted io.StringWriter, word string) (bool, error) {
	// It would be nicer to write this using tail recursion, but Go doesn't
	// support tail call optimization. Hence use this alternating approach.

	// Prefer single-quotes.
	pos, err := singleQuoteRange(quoted, word, 0)
	if err != nil {
		return false, err
	}
	if pos < 0 { // word contains only regular bytes that don't need to be quoted
		return false, nil
	}

	// Now start alternating between the two ways of quoting words until the
	// word is completely quoted. Start with double-quotes.
	for quoteRange, nextQuoteRange := doubleQuoteRange, singleQuoteRange; pos < len(word); {
		pos, err = quoteRange(quoted, word, pos)
		if err != nil {
			return false, err
		}
		prevQuoteRange := quoteRange
		quoteRange, nextQuoteRange = nextQuoteRange, prevQuoteRange
	}

	return true, nil
}

// Writes the quoted word to quoted using single-quotes, starting from pos.
// Tries to quote as much as possible, until a byte is encountered that cannot
// be portably quoted using single-quotes, the end of word is reached, or an
// error occurs. Returns the first unquoted position in word, or -1 if word
// contains only regular bytes that don't need to be quoted. In that case,
// nothing is written to quoted at all.
func singleQuoteRange(quoted io.StringWriter, word string, pos int) (int, error) {
	needsQuotes, pendingPos := false, pos

	// Iterate over word byte-by-byte, not rune-by-rune.
	// It doesn't have to be valid UTF-8, in the end.
loop:
	for ; pos < len(word); pos++ {
		class, err := quoteClassOf(word[pos])
		switch class {
		case regular:
			continue

		case special, specialNonPreserved, backtick:
			needsQuotes = true

		case singleQuote:
			needsQuotes = true
			break loop

		default:
			return 0, &wordErr{pos, err}
		}
	}

	if !needsQuotes {
		return -1, nil
	}

	if pos > 0 {
		if _, err := quoted.WriteString("'"); err != nil {
			return 0, err
		}
		if _, err := quoted.WriteString(word[pendingPos:pos]); err != nil {
			return 0, err
		}
		if _, err := quoted.WriteString("'"); err != nil {
			return 0, err
		}
	}

	return pos, nil
}

// Writes the quoted word to quoted using double-quotes, starting from pos.
// Tries to quote as much as possible, until a byte is encountered that cannot
// be portably quoted using double-quotes, the end of word is reached, or an
// error occurs. Returns the first unquoted position in word.
func doubleQuoteRange(quoted io.StringWriter, word string, pos int) (int, error) {
	if _, err := quoted.WriteString(`"`); err != nil {
		return 0, err
	}

	pendingPos := pos

	// Iterate over word byte-by-byte, not rune-by-rune.
	// It doesn't have to be valid UTF-8, in the end.
loop:
	for ; pos < len(word); pos++ {
		class, err := quoteClassOf(word[pos])
		switch class {
		case regular, special, singleQuote:
			continue

		case specialNonPreserved:
			if _, err := quoted.WriteString(word[pendingPos:pos]); err != nil {
				return 0, err
			}
			// A <backslash> [...] shall preserve the literal value of the following character, [...]
			// https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_02_01
			if _, err := quoted.WriteString(`\`); err != nil {
				return 0, err
			}
			pendingPos = pos

		// The fish shell doesn't support the old backtick syntax for command
		// substitution, and hence it won't unescape an escaped backtick in
		// double-quotes. Stop double-quoting here. This will switch back to use
		// single-quotes, in which backticks don't need to be escaped at all.
		// This is more portable than trying to get away with an escaped
		// backtick.
		case backtick:
			break loop

		default:
			return 0, &wordErr{pos, err}
		}
	}

	if _, err := quoted.WriteString(word[pendingPos:pos]); err != nil {
		return 0, err
	}
	if _, err := quoted.WriteString(`"`); err != nil {
		return 0, err
	}
	return pos, nil
}

type quoteClass byte

const (
	// A regular byte that doesn't need to be quoted or otherwise escaped.
	regular quoteClass = iota

	// A special byte that needs to be quoted.
	special

	// A special byte that needs to be quoted like [special], and additionally
	// needs to be escaped when enclosed by double-quotes.
	specialNonPreserved

	// The single-quote.
	singleQuote

	// The backtick.
	backtick

	// Some byte that may not occur in any shell word.
	illegal
)

// Returns the [quoteClass] of b. If the quoteClass is [illegal], the error will
// be non-nil and describe why the byte is illegal.
func quoteClassOf(b byte) (quoteClass, error) {
	// Non-POSIX: Treat all extended ASCII as special. There's some weirdness
	// when using bash with a libc that has an unconventional implementation of
	// isblank. Bash uses isblank to check if a given byte is whitespace and
	// thus a word separator. For example, on macOS, when the locale is set to
	// UTF-8, isblank returns true for '\xa0', which is a non-breaking space in
	// CP-1252 / ISO Latin-1. The single '\xa0' isn't even valid UTF-8 to begin
	// with. For this reason, it's safer to treat all extended ASCII as special,
	// just in case there are other weird combinations that would trigger the
	// problem described above.
	// https://docs.rs/shlex/1.3.0/shlex/quoting_warning/#solved-xa0
	if int(b) >= len(asciiQuoteClasses) {
		return special, nil
	}

	c := asciiQuoteClasses[b]
	if c == illegal {
		// Either a NUL byte or another illegal control byte
		if b == 0 {
			return illegal, nulByteErr{}
		}

		return illegal, controlByteErr(b)
	}

	return c, nil
}

// A lookup table for the [quoteClass] of each ASCII byte.
// To be used via [quoteClassOf].
var asciiQuoteClasses = func() (table [128]quoteClass) {
	for b := range table {
		switch b {
		case
			// POSIX: The application shall quote the following characters if
			// they are to represent themselves:
			// https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_02
			'|', '&', ';', '<', '>', '(', ')' /*, '$', '`', '\\', '"', '\''*/, ' ', '\t', '\n',

			// POSIX: [...] these characters may be special depending on
			// conditions described elsewhere [...]
			// https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_02
			'*', '?', '[', '#', '~', '=', '%', // Don't do any lexical analysis, just treat those as always special

			// Non-POSIX: Also allow carriage return, in case something uses DOS
			// line endings.
			'\r',

			// Non-POSIX: Used in interactive shells. It's safer to escape them.
			// https://docs.rs/shlex/1.3.0/shlex/quoting_warning/#solved--and-
			'!', '^',

			// Non-POSIX: Some shells (e.g. bash, zsh) have brace expansion, and
			// wordexp(3) will fail with WRDE_BADCHAR.
			'{', '}':
			table[b] = special

		// POSIX: Enclosing characters in double-quotes shall preserve the
		// literal value of all characters within the double-quotes, with the
		// exception of the characters backquote, <dollar-sign>, and
		// <backslash>. [...] The <backslash> shall retain its special meaning
		// as an escape character.
		// https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_02_03
		case '$' /* , '`' */, '\\', '"':
			table[b] = specialNonPreserved

		case '\'':
			table[b] = singleQuote

		case '`':
			table[b] = backtick

		default:
			// Non-POSIX: Control bytes (except LF, CR and HT, which are handled
			// above) are evil for interactive shells. Especially, NUL bytes are
			// evil in virtually all use cases, even for non-interactive shells.
			// https://docs.rs/shlex/1.3.0/shlex/quoting_warning/#nul-bytes
			// https://docs.rs/shlex/1.3.0/shlex/quoting_warning/#control-characters-interactive-contexts-only
			if b < 32 || b == 127 {
				table[b] = illegal
			}
		}
	}
	return
}()

type wordErr struct {
	pos int
	err error
}

func (e *wordErr) Error() string { return fmt.Sprintf("byte %d: %v", e.pos, e.err) }
func (e *wordErr) Unwrap() error { return e.err }

type nulByteErr struct{}

func (nulByteErr) Error() string { return "NUL bytes cannot be processed by virtually any shell" }

type controlByteErr byte

func (e controlByteErr) Error() string {
	return fmt.Sprintf("control byte 0x%02x cannot be safely quoted in interactive shells", byte(e))
}
